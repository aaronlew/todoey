//
//  Item.swift
//  Todoey
//
//  Created by Aaron Lew on 11/5/18.
//  Copyright © 2018 Chia Pang Lew. All rights reserved.
//

import Foundation

class Item: Codable {
    var title : String = ""
    var done : Bool = false
}
